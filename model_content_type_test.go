package remailer_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/remailer"
)

func TestModelContentTypeSuccess(t *testing.T) {
	mdl := remailer.ContentTypeHTML

	err := mdl.Validate()

	assert.NoError(t, err, "[TestModelContentTypeSuccess] Should not error")
}

func TestModelContentTypeErrorInvalid(t *testing.T) {
	mdl := remailer.ContentType("test-invalid")

	err := mdl.Validate()

	assert.Error(t, err, "[TestModelContentTypeErrorInvalid] Should error")
	assert.Equal(t, remailer.ErrContentTypeInvalid, err, "[TestModelContentTypeErrorInvalid] Error should \"content-type: Invalid or not supported yet\"")
}
