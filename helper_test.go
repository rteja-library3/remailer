package remailer_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/remailer"
)

func TestMustBuildBody(t *testing.T) {
	result := remailer.MustBuildBody(remailer.Body{
		ContentType: remailer.ContentTypeHTML,
		Message:     []byte("test"),
	})

	assert.NotEmpty(t, result, "[TestMustBuildBody] Result is not empty")
	assert.Equal(t, "test", result, "[TestMustBuildBody] Result should \"test\"")
}

func TestBuildRecepient(t *testing.T) {
	recepients := []remailer.Recepient{
		{
			Name:    "",
			Address: "aa@aa.com",
		},
		{
			Name:    "Test",
			Address: "test@aa.com",
		},
	}

	result := remailer.BuildRecepient(recepients)

	assert.NotEmpty(t, result, "[TestBuildRecepient] Result should not empty")
	assert.Len(t, result, 2, "[TestBuildRecepient] Result len should 2")
	assert.Equal(t, "aa@aa.com", result[0], "[TestBuildRecepient] Result[0] should \"aa@aa.com\"")
	assert.Equal(t, "Test <test@aa.com>", result[1], "[TestBuildRecepient] Result[1] should \"Test <test@aa.com>\"")
}
