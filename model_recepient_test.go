package remailer_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/remailer"
)

func TestModelRecepientSuccess(t *testing.T) {
	mdl := remailer.Recepient{
		Name:    "test",
		Address: "aa@aa.com",
	}

	err := mdl.Validate()

	assert.NoError(t, err, "[TestModelRecepientSuccess] Should not error")
}

func TestModelRecepientErrAddressInvalid(t *testing.T) {
	mdl := remailer.Recepient{
		Name:    "test",
		Address: "test",
	}

	err := mdl.Validate()

	assert.Error(t, err, "[TestModelRecepientErrAddressInvalid] Should error")
	assert.Equal(t, remailer.ErrRecepientAddressInvalid, err, "[TestModelRecepientErrAddressInvalid] Error should \"recepient: Address is invalid\"")
}
