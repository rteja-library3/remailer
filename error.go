package remailer

import (
	"fmt"
)

var (
	ErrEmailNotFound                             error = fmt.Errorf("email: Not found")
	ErrSenderIsRequired                          error = fmt.Errorf("email: Sender is required")
	ErrConfigNotFound                            error = fmt.Errorf("email: Config found")
	ErrCodeMessageRejected                       error = fmt.Errorf("email: Messasge Rejected")
	ErrCodeMailFromDomainNotVerifiedException    error = fmt.Errorf("email: Mail From Domain Not Verified")
	ErrCodeConfigurationSetDoesNotExistException error = fmt.Errorf("email: Configuration Set Does Not Exist")
)

// attachment
var (
	ErrAttachmentFilenameIsRequired error = fmt.Errorf("attachment: Filename is required")
	ErrAttachmentBodyIsRequired     error = fmt.Errorf("attachment: Body is required")
)

// content type
var (
	ErrContentTypeInvalid error = fmt.Errorf("content-type: Invalid or not supported yet")
)

// body
var (
	ErrBodyMessageIsRequired error = fmt.Errorf("body: Message is required")
)

// recepient
var (
	ErrRecepientAddressInvalid error = fmt.Errorf("recepient: Address is invalid")
)

// message
var (
	ErrMessageDestinationIsRequired error = fmt.Errorf("message: Destination is required")
	ErrMessageFromInvalid           error = fmt.Errorf("message: From is invalid")
)
