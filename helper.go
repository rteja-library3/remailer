package remailer

func BuildRecepient(recepients []Recepient) (copyOfRecepient []string) {
	copyOfRecepient = make([]string, len(recepients))

	for row, recepient := range recepients {
		if recepient.Name != "" {
			copyOfRecepient[row] = recepient.Name + " <" + recepient.Address + ">"
		} else {
			copyOfRecepient[row] = recepient.Address
		}
	}

	return
}

func MustBuildBody(body Body) string {
	return string(body.Message)
}
