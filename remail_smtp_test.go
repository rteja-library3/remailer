package remailer_test

import (
	"context"
	"net/smtp"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/remailer"
)

func TestCreateSMTPSender(t *testing.T) {
	smtp := remailer.NewSMTPSender(smtp.PlainAuth("", "", "", ""), "", "", "", nil)

	assert.NotNil(t, smtp, "[TestCreateSMTPSender] Should not nil")
}

func TestSMTPSendSuccess(t *testing.T) {
	client := func(addr string, a smtp.Auth, from string, to []string, msg []byte) error {
		return nil
	}

	smtp := remailer.NewSMTPSender(smtp.PlainAuth("", "", "", ""), "aa@aa.com", "", "", client)

	msg := remailer.Message{
		From:    "",
		Subject: "test",
		To: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Cc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Bcc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Body: remailer.Body{
			ContentType: remailer.ContentTypePlaintext,
			Message:     []byte("test"),
		},
		Attachments: []remailer.Attachment{
			{
				Filename: "test",
				Body:     []byte("test"),
			},
		},
	}

	err := smtp.Send(context.Background(), msg)

	assert.NoError(t, err, "[TestSMTPSendSuccess] Should not error")
}

func TestSMTPSendError(t *testing.T) {
	client := func(addr string, a smtp.Auth, from string, to []string, msg []byte) error {
		return nil
	}

	smtp := remailer.NewSMTPSender(smtp.PlainAuth("", "", "", ""), "", "", "", client)

	msg := remailer.Message{
		From:    "",
		Subject: "test",
		To: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Cc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Bcc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Body: remailer.Body{
			ContentType: remailer.ContentTypePlaintext,
			Message:     []byte("test"),
		},
		Attachments: []remailer.Attachment{
			{
				Filename: "test",
				Body:     []byte("test"),
			},
		},
	}

	err := smtp.Send(context.Background(), msg)

	assert.Error(t, err, "[TestSMTPSendError] Should error")
	assert.Equal(t, remailer.ErrMessageFromInvalid, err, "[TestSMTPSendError] Error should \"message: From is invalid\"")
}
