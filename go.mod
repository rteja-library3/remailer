module gitlab.com/rteja-library3/remailer

go 1.17

require (
	github.com/aws/aws-sdk-go v1.43.6
	github.com/stretchr/testify v1.7.0
	gitlab.com/rteja-library3/rhelper v0.0.0-20220216064332-0dfcfb216ad0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-chi/chi v1.5.4 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/oklog/ulid/v2 v2.0.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/stretchr/objx v0.3.0 // indirect
	gitlab.com/rteja-library3/rencryption v0.0.0-20220215103608-74b4d02b0678 // indirect
	go.mongodb.org/mongo-driver v1.8.3 // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
