package remailer_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/remailer"
)

func TestModelMessageSuccess(t *testing.T) {
	mdl := remailer.Message{
		From:    "aa@aa.com",
		Subject: "test",
		To: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Cc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Bcc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Body: remailer.Body{
			ContentType: remailer.ContentTypePlaintext,
			Message:     []byte("test"),
		},
		Attachments: []remailer.Attachment{
			{
				Filename: "test",
				Body:     []byte("test"),
			},
		},
	}

	err := mdl.Validate()

	assert.NoError(t, err, "[TestModelMessageSuccess] Should not error")
}

func TestModelMessageErrFromInvalid(t *testing.T) {
	mdl := remailer.Message{
		From:    "",
		Subject: "test",
		To: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Cc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Bcc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Body: remailer.Body{
			ContentType: remailer.ContentTypePlaintext,
			Message:     []byte("test"),
		},
		Attachments: []remailer.Attachment{
			{
				Filename: "test",
				Body:     []byte("test"),
			},
		},
	}

	err := mdl.Validate()

	assert.Error(t, err, "[TestModelMessageErrFromInvalid] Should error")
	assert.Equal(t, remailer.ErrMessageFromInvalid, err, "[TestModelMessageErrFromInvalid] Error should \"message: From is invalid\"")
}

func TestModelMessageErrDestinationIsRequired(t *testing.T) {
	mdl := remailer.Message{
		From:    "aa@aa.com",
		Subject: "test",
		To:      []remailer.Recepient{},
		Cc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Bcc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Body: remailer.Body{
			ContentType: remailer.ContentTypePlaintext,
			Message:     []byte("test"),
		},
		Attachments: []remailer.Attachment{
			{
				Filename: "test",
				Body:     []byte("test"),
			},
		},
	}

	err := mdl.Validate()

	assert.Error(t, err, "[TestModelMessageErrDestinationIsRequired] Should error")
	assert.Equal(t, remailer.ErrMessageDestinationIsRequired, err, "[TestModelMessageErrDestinationIsRequired] Error should \"message: Destination is required\"")
}

func TestModelMessageErrBody(t *testing.T) {
	mdl := remailer.Message{
		From:    "aa@aa.com",
		Subject: "test",
		To: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Cc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Bcc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Body: remailer.Body{
			ContentType: remailer.ContentTypePlaintext,
			Message:     nil,
		},
		Attachments: []remailer.Attachment{
			{
				Filename: "test",
				Body:     []byte("test"),
			},
		},
	}

	err := mdl.Validate()

	assert.Error(t, err, "[TestModelMessageErrBody] Should error")
	assert.Equal(t, remailer.ErrBodyMessageIsRequired, err, "[TestModelMessageErrBody] Error should \"body: Message is required\"")
}

func TestModelMessageErrTo(t *testing.T) {
	mdl := remailer.Message{
		From:    "aa@aa.com",
		Subject: "test",
		To: []remailer.Recepient{
			{
				Name:    "",
				Address: "test",
			},
		},
		Cc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Bcc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Body: remailer.Body{
			ContentType: remailer.ContentTypePlaintext,
			Message:     []byte("test"),
		},
		Attachments: []remailer.Attachment{
			{
				Filename: "test",
				Body:     []byte("test"),
			},
		},
	}

	err := mdl.Validate()

	assert.Error(t, err, "[TestModelMessageErrTo] Should error")
	assert.Equal(t, remailer.ErrRecepientAddressInvalid, err, "[TestModelMessageErrTo] Error should \"recepient: Address is invalid\"")
}

func TestModelMessageErrCc(t *testing.T) {
	mdl := remailer.Message{
		From:    "aa@aa.com",
		Subject: "test",
		To: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Cc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa",
			},
		},
		Bcc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Body: remailer.Body{
			ContentType: remailer.ContentTypePlaintext,
			Message:     []byte("test"),
		},
		Attachments: []remailer.Attachment{
			{
				Filename: "test",
				Body:     []byte("test"),
			},
		},
	}

	err := mdl.Validate()

	assert.Error(t, err, "[TestModelMessageErrCc] Should error")
	assert.Equal(t, remailer.ErrRecepientAddressInvalid, err, "[TestModelMessageErrCc] Error should \"recepient: Address is invalid\"")
}

func TestModelMessageErrBcc(t *testing.T) {
	mdl := remailer.Message{
		From:    "aa@aa.com",
		Subject: "test",
		To: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Cc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Bcc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa",
			},
		},
		Body: remailer.Body{
			ContentType: remailer.ContentTypePlaintext,
			Message:     []byte("test"),
		},
		Attachments: []remailer.Attachment{
			{
				Filename: "test",
				Body:     []byte("test"),
			},
		},
	}

	err := mdl.Validate()

	assert.Error(t, err, "[TestModelMessageErrBcc] Should error")
	assert.Equal(t, remailer.ErrRecepientAddressInvalid, err, "[TestModelMessageErrBcc] Error should \"recepient: Address is invalid\"")
}

func TestModelMessageErrAttachment(t *testing.T) {
	mdl := remailer.Message{
		From:    "aa@aa.com",
		Subject: "test",
		To: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Cc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Bcc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Body: remailer.Body{
			ContentType: remailer.ContentTypePlaintext,
			Message:     []byte("test"),
		},
		Attachments: []remailer.Attachment{
			{
				Filename: "test",
				Body:     nil,
			},
		},
	}

	err := mdl.Validate()

	assert.Error(t, err, "[TestModelMessageErrAttachment] Should error")
	assert.Equal(t, remailer.ErrAttachmentBodyIsRequired, err, "[TestModelMessageErrAttachment] Error should \"attachment: Body is required\"")
}
