package remailer

import "gitlab.com/rteja-library3/rhelper"

type Message struct {
	From        string
	Subject     string
	To          []Recepient
	Cc          []Recepient
	Bcc         []Recepient
	Body        Body
	Attachments []Attachment
}

func (r Message) Validate() (err error) {
	if !rhelper.IsEmailValid(r.From) {
		err = ErrMessageFromInvalid
		return
	}

	if len(r.To) == 0 {
		err = ErrMessageDestinationIsRequired
		return
	}

	err = r.Body.Validate()
	if err != nil {
		return
	}

	for _, recepient := range r.To {
		err = recepient.Validate()
		if err != nil {
			return
		}
	}

	for _, recepient := range r.Cc {
		err = recepient.Validate()
		if err != nil {
			return
		}
	}

	for _, recepient := range r.Bcc {
		err = recepient.Validate()
		if err != nil {
			return
		}
	}

	for _, attachment := range r.Attachments {
		err = attachment.Validate()
		if err != nil {
			return
		}
	}

	return
}
