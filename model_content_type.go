package remailer

type ContentType string

func (c ContentType) Validate() (err error) {
	if c != ContentTypePlaintext && c != ContentTypeHTML {
		err = ErrContentTypeInvalid
		return
	}

	return
}
