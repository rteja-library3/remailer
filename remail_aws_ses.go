package remailer

import (
	"context"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/ses"
)

type awsSes struct {
	config        *aws.Config
	charSet       string
	defaultSender string
	clientBuilder SESClientBuilder
}

func NewAWSSes(charSet, sender string, config *aws.Config, clientBuilder SESClientBuilder) Remail {
	if clientBuilder == nil {
		clientBuilder = DefaultSESClientBuilder
	}

	return &awsSes{
		config:        config,
		charSet:       charSet,
		defaultSender: sender,
		clientBuilder: clientBuilder,
	}
}

func (a awsSes) Send(ctx context.Context, message Message) (err error) {
	if message.From == "" {
		message.From = a.defaultSender
	}

	err = message.Validate()
	if err != nil {
		return err
	}

	svc, err := a.clientBuilder(a.config)
	if err != nil {
		return err
	}

	var body *ses.Body
	if message.Body.ContentType == ContentTypeHTML {
		body = &ses.Body{
			Html: &ses.Content{
				Charset: aws.String(a.charSet),
				Data:    aws.String(MustBuildBody(message.Body)),
			},
		}
	} else if message.Body.ContentType == ContentTypePlaintext {
		body = &ses.Body{
			Text: &ses.Content{
				Charset: aws.String(a.charSet),
				Data:    aws.String(MustBuildBody(message.Body)),
			},
		}
	}

	input := &ses.SendEmailInput{
		Destination: &ses.Destination{
			ToAddresses:  aws.StringSlice(BuildRecepient(message.To)),
			CcAddresses:  aws.StringSlice(BuildRecepient(message.Cc)),
			BccAddresses: aws.StringSlice(BuildRecepient(message.Bcc)),
		},
		Message: &ses.Message{
			Body: body,
			Subject: &ses.Content{
				Charset: aws.String(a.charSet),
				Data:    aws.String(message.Subject),
			},
		},
		Source: aws.String(message.From),
	}

	// Attempt to send the email.
	_, err = svc.SendEmailWithContext(ctx, input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case ses.ErrCodeMessageRejected:
				err = ErrCodeMessageRejected
				return
			case ses.ErrCodeMailFromDomainNotVerifiedException:
				err = ErrCodeMailFromDomainNotVerifiedException
				return
			case ses.ErrCodeConfigurationSetDoesNotExistException:
				err = ErrCodeConfigurationSetDoesNotExistException
				return
			default:
				err = aerr
				return
			}
		}
	}

	return
}
