package remailer_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/remailer"
)

func TestModelBodySuccess(t *testing.T) {
	mdl := remailer.Body{
		ContentType: remailer.ContentTypePlaintext,
		Message:     []byte("test"),
	}
	err := mdl.Validate()

	assert.NoError(t, err, "[TestModelBodySuccess] Should not error")
}

func TestModelBodyErrorContentTypeInvalid(t *testing.T) {
	mdl := remailer.Body{
		ContentType: remailer.ContentType("test"),
		Message:     []byte("test"),
	}
	err := mdl.Validate()

	assert.Error(t, err, "[TestModelBodyErrorContentTypeInvalid] Should error")
	assert.Equal(t, remailer.ErrContentTypeInvalid, err, "[TestModelBodyErrorContentTypeInvalid] Error should \"content-type: Invalid or not supported yet\"")
}

func TestModelBodyErrorMessageIsRequired(t *testing.T) {
	mdl := remailer.Body{
		ContentType: remailer.ContentTypePlaintext,
		Message:     nil,
	}
	err := mdl.Validate()

	assert.Error(t, err, "[TestModelBodyErrorMessageIsRequired] Should error")
	assert.Equal(t, remailer.ErrBodyMessageIsRequired, err, "[TestModelBodyErrorMessageIsRequired] Error should \"body: Message is required\"")
}
