package remailer

import "gitlab.com/rteja-library3/rhelper"

type Recepient struct {
	Name    string
	Address string
}

func (r Recepient) Validate() (err error) {
	if !rhelper.IsEmailValid(r.Address) {
		err = ErrRecepientAddressInvalid
		return
	}

	return
}
