package remailer_test

import (
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/remailer"
)

func TestDefaultSESClientBuilder(t *testing.T) {
	cfg := &aws.Config{}

	client, err := remailer.DefaultSESClientBuilder(cfg)

	assert.NoError(t, err, "[TestDefaultSESClientBuilder] Should not error")
	assert.NotEmpty(t, client, "[TestDefaultSESClientBuilder] Should not empty")
}
