package remailer

type Body struct {
	ContentType ContentType
	Message     []byte
}

func (r Body) Validate() (err error) {
	err = r.ContentType.Validate()
	if err != nil {
		return
	}

	if len(r.Message) == 0 {
		err = ErrBodyMessageIsRequired
		return
	}

	return
}
