package remailer

type Attachment struct {
	Filename string
	Body     []byte
}

func (r Attachment) Validate() (err error) {
	if r.Filename == "" {
		err = ErrAttachmentFilenameIsRequired
		return
	}

	if len(r.Body) == 0 {
		err = ErrAttachmentBodyIsRequired
		return
	}

	return
}
