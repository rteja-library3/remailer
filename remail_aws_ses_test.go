package remailer_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ses"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/rteja-library3/remailer"
	"gitlab.com/rteja-library3/remailer/mocks"
)

type errSES struct {
	code string
}

func (e errSES) Code() string {
	return e.code
}

func (e errSES) Message() string {
	return e.code
}

func (e errSES) OrigErr() error {
	return fmt.Errorf(e.code)
}

func (e errSES) Error() string {
	return e.code
}

func TestCreateSESClient(t *testing.T) {
	ses := remailer.NewAWSSes("", "", aws.NewConfig(), nil)

	assert.NotNil(t, ses, "[TestCreateSESClient] Should not nil")
}

func TestSESClientSuccessPlainText(t *testing.T) {
	sesMockClient := new(mocks.SESClient)
	sesMockClient.On("SendEmailWithContext", mock.Anything, mock.Anything).
		Return(nil, nil)

	cl := func(configs ...*aws.Config) (client remailer.SESClient, err error) {
		return sesMockClient, nil
	}

	ses := remailer.NewAWSSes("", "aa@aa.com", aws.NewConfig(), cl)

	msg := remailer.Message{
		From:    "",
		Subject: "test",
		To: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Cc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Bcc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Body: remailer.Body{
			ContentType: remailer.ContentTypePlaintext,
			Message:     []byte("test"),
		},
		Attachments: []remailer.Attachment{
			{
				Filename: "test",
				Body:     []byte("test"),
			},
		},
	}

	err := ses.Send(context.Background(), msg)

	assert.NoError(t, err, "[TestSESClientSuccessPlainText] Should no error")
}

func TestSESClientSuccessHTML(t *testing.T) {
	sesMockClient := new(mocks.SESClient)
	sesMockClient.On("SendEmailWithContext", mock.Anything, mock.Anything).
		Return(nil, nil)

	cl := func(configs ...*aws.Config) (client remailer.SESClient, err error) {
		return sesMockClient, nil
	}

	ses := remailer.NewAWSSes("", "aa@aa.com", aws.NewConfig(), cl)

	msg := remailer.Message{
		From:    "",
		Subject: "test",
		To: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Cc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Bcc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Body: remailer.Body{
			ContentType: remailer.ContentTypeHTML,
			Message:     []byte("test"),
		},
		Attachments: []remailer.Attachment{
			{
				Filename: "test",
				Body:     []byte("test"),
			},
		},
	}

	err := ses.Send(context.Background(), msg)

	assert.NoError(t, err, "[TestSESClientSuccessHTML] Should no error")
}

func TestSESClientErrorValidate(t *testing.T) {
	sesMockClient := new(mocks.SESClient)

	cl := func(configs ...*aws.Config) (client remailer.SESClient, err error) {
		return sesMockClient, nil
	}

	ses := remailer.NewAWSSes("", "", aws.NewConfig(), cl)

	msg := remailer.Message{
		From:    "",
		Subject: "test",
		To: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Cc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Bcc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Body: remailer.Body{
			ContentType: remailer.ContentTypeHTML,
			Message:     []byte("test"),
		},
		Attachments: []remailer.Attachment{
			{
				Filename: "test",
				Body:     []byte("test"),
			},
		},
	}

	err := ses.Send(context.Background(), msg)

	assert.Error(t, err, "[TestSESClientErrorValidate] Should error")
	assert.Equal(t, remailer.ErrMessageFromInvalid, err, "[TestSESClientErrorValidate] Error should \"message: From is invalid\"")
}

func TestSESClientErrorCreation(t *testing.T) {
	cl := func(configs ...*aws.Config) (client remailer.SESClient, err error) {
		return nil, fmt.Errorf("test")
	}

	ses := remailer.NewAWSSes("", "aa@aa.com", aws.NewConfig(), cl)

	msg := remailer.Message{
		From:    "",
		Subject: "test",
		To: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Cc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Bcc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Body: remailer.Body{
			ContentType: remailer.ContentTypeHTML,
			Message:     []byte("test"),
		},
		Attachments: []remailer.Attachment{
			{
				Filename: "test",
				Body:     []byte("test"),
			},
		},
	}

	err := ses.Send(context.Background(), msg)

	assert.Error(t, err, "[TestSESClientErrorCreation] Should error")
	assert.Equal(t, fmt.Errorf("test"), err, "[TestSESClientErrorCreation] Error should \"test\"")
}

func TestSESClientErrMsgRejected(t *testing.T) {
	sesMockClient := new(mocks.SESClient)

	sesMockClient.On("SendEmailWithContext", mock.Anything, mock.Anything).
		Return(nil, errSES{ses.ErrCodeMessageRejected})

	cl := func(configs ...*aws.Config) (client remailer.SESClient, err error) {
		return sesMockClient, nil
	}

	ses := remailer.NewAWSSes("", "aa@aa.com", aws.NewConfig(), cl)

	msg := remailer.Message{
		From:    "",
		Subject: "test",
		To: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Cc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Bcc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Body: remailer.Body{
			ContentType: remailer.ContentTypeHTML,
			Message:     []byte("test"),
		},
		Attachments: []remailer.Attachment{
			{
				Filename: "test",
				Body:     []byte("test"),
			},
		},
	}

	err := ses.Send(context.Background(), msg)

	assert.Error(t, err, "[TestSESClientErrMsgRejected] Should error")
	assert.Equal(t, remailer.ErrCodeMessageRejected, err, "[TestSESClientErrMsgRejected] Error should \"email: Messasge Rejected\"")
}

func TestSESClientErrCodeMailFromDomainNotVerifiedException(t *testing.T) {
	sesMockClient := new(mocks.SESClient)

	sesMockClient.On("SendEmailWithContext", mock.Anything, mock.Anything).
		Return(nil, errSES{ses.ErrCodeMailFromDomainNotVerifiedException})

	cl := func(configs ...*aws.Config) (client remailer.SESClient, err error) {
		return sesMockClient, nil
	}

	ses := remailer.NewAWSSes("", "aa@aa.com", aws.NewConfig(), cl)

	msg := remailer.Message{
		From:    "",
		Subject: "test",
		To: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Cc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Bcc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Body: remailer.Body{
			ContentType: remailer.ContentTypeHTML,
			Message:     []byte("test"),
		},
		Attachments: []remailer.Attachment{
			{
				Filename: "test",
				Body:     []byte("test"),
			},
		},
	}

	err := ses.Send(context.Background(), msg)

	assert.Error(t, err, "[TestSESClientErrCodeMailFromDomainNotVerifiedException] Should error")
	assert.Equal(t, remailer.ErrCodeMailFromDomainNotVerifiedException, err, "[TestSESClientErrCodeMailFromDomainNotVerifiedException] Error should \"email: Mail From Domain Not Verified\"")
}

func TestSESClientErrCodeConfigurationSetDoesNotExistException(t *testing.T) {
	sesMockClient := new(mocks.SESClient)

	sesMockClient.On("SendEmailWithContext", mock.Anything, mock.Anything).
		Return(nil, errSES{ses.ErrCodeConfigurationSetDoesNotExistException})

	cl := func(configs ...*aws.Config) (client remailer.SESClient, err error) {
		return sesMockClient, nil
	}

	ses := remailer.NewAWSSes("", "aa@aa.com", aws.NewConfig(), cl)

	msg := remailer.Message{
		From:    "",
		Subject: "test",
		To: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Cc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Bcc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Body: remailer.Body{
			ContentType: remailer.ContentTypeHTML,
			Message:     []byte("test"),
		},
		Attachments: []remailer.Attachment{
			{
				Filename: "test",
				Body:     []byte("test"),
			},
		},
	}

	err := ses.Send(context.Background(), msg)

	assert.Error(t, err, "[TestSESClientErrCodeConfigurationSetDoesNotExistException] Should error")
	assert.Equal(t, remailer.ErrCodeConfigurationSetDoesNotExistException, err, "[TestSESClientErrCodeConfigurationSetDoesNotExistException] Error should \"email: Configuration Set Does Not Exist\"")
}

func TestSESClientErrOther(t *testing.T) {
	sesMockClient := new(mocks.SESClient)

	sesMockClient.On("SendEmailWithContext", mock.Anything, mock.Anything).
		Return(nil, errSES{"test"})

	cl := func(configs ...*aws.Config) (client remailer.SESClient, err error) {
		return sesMockClient, nil
	}

	ses := remailer.NewAWSSes("", "aa@aa.com", aws.NewConfig(), cl)

	msg := remailer.Message{
		From:    "",
		Subject: "test",
		To: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Cc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Bcc: []remailer.Recepient{
			{
				Name:    "",
				Address: "aa@aa.com",
			},
		},
		Body: remailer.Body{
			ContentType: remailer.ContentTypeHTML,
			Message:     []byte("test"),
		},
		Attachments: []remailer.Attachment{
			{
				Filename: "test",
				Body:     []byte("test"),
			},
		},
	}

	err := ses.Send(context.Background(), msg)

	assert.Error(t, err, "[TestSESClientErrCodeConfigurationSetDoesNotExistException] Should error")
	assert.Equal(t, "test", err.Error(), "[TestSESClientErrCodeConfigurationSetDoesNotExistException] Error should \"test\"")
}
