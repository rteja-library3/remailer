package remailer

import (
	"bytes"
	"context"
	"encoding/base64"
	"fmt"
	"mime/multipart"
	"net/http"
	"net/smtp"
	"strings"
)

type smtpSender struct {
	auth          smtp.Auth
	defaultSender string
	host          string
	port          string
	clientBuilder SMTPClient
}

func NewSMTPSender(auth smtp.Auth, sender, host, port string, clientBuilder SMTPClient) Remail {
	if clientBuilder == nil {
		clientBuilder = DefaultSMTPClient
	}

	return &smtpSender{
		auth,
		sender,
		host,
		port,
		clientBuilder,
	}
}

func (s smtpSender) Send(ctx context.Context, message Message) (err error) {
	if message.From == "" {
		message.From = s.defaultSender
	}

	err = message.Validate()
	if err != nil {
		return err
	}

	withAttachment := len(message.Attachments) > 0

	buf := bytes.NewBuffer(nil)

	buf.WriteString("MIME-Version: 1.0\n")
	buf.WriteString(fmt.Sprintf("Subject: %s\n", message.Subject))
	buf.WriteString(fmt.Sprintf("To: %s\n", strings.Join(BuildRecepient(message.To), ",")))

	if len(message.Cc) > 0 {
		buf.WriteString(fmt.Sprintf("Cc: %s\n", strings.Join(BuildRecepient(message.Cc), ",")))
	}

	if len(message.Bcc) > 0 {
		buf.WriteString(fmt.Sprintf("Bcc: %s\n", strings.Join(BuildRecepient(message.Bcc), ",")))
	}

	writer := multipart.NewWriter(buf)
	boundary := writer.Boundary()

	if withAttachment {
		buf.WriteString(fmt.Sprintf("Content-Type: multipart/mixed; boundary=%s\r\n", boundary))
		buf.WriteString(fmt.Sprintf("\r\n--%s\r\n", boundary))
	}

	buf.WriteString(fmt.Sprintf("Content-Type: %s; charset=\"utf-8\"\r\n", message.Body.ContentType))
	buf.WriteString("\r\n" + MustBuildBody(message.Body))

	if withAttachment {
		for _, v := range message.Attachments {
			buf.WriteString(fmt.Sprintf("\n\n--%s\n", boundary))
			buf.WriteString(fmt.Sprintf("Content-Type: %s\n", http.DetectContentType(v.Body)))
			buf.WriteString("Content-Transfer-Encoding: base64\n")
			buf.WriteString(fmt.Sprintf("Content-Disposition: attachment; filename=%s\n", v.Filename))

			b := make([]byte, base64.StdEncoding.EncodedLen(len(v.Body)))
			base64.StdEncoding.Encode(b, v.Body)
			buf.Write(b)
			buf.WriteString(fmt.Sprintf("\n--%s", boundary))
		}

		buf.WriteString("--")
	}

	err = s.clientBuilder(
		fmt.Sprintf("%s:%s", s.host, s.port),
		s.auth,
		message.From,
		BuildRecepient(message.To),
		buf.Bytes(),
	)
	return
}
