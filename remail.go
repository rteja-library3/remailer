package remailer

import (
	"context"
)

type Remail interface {
	Send(ctx context.Context, messages Message) (err error)
}
