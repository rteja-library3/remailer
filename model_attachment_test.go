package remailer_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/remailer"
)

func TestModelAttachmentSuccess(t *testing.T) {
	mdl := remailer.Attachment{
		Filename: "test.txt",
		Body:     []byte("test"),
	}

	err := mdl.Validate()

	assert.NoError(t, err, "[TestModelAttachmentSuccess] Should not error")
}

func TestModelAttachmentErrFilenameIsRequired(t *testing.T) {
	mdl := remailer.Attachment{
		Filename: "",
		Body:     []byte("test"),
	}

	err := mdl.Validate()

	assert.Error(t, err, "[TestModelAttachmentSuccess] Should error")
	assert.Equal(t, remailer.ErrAttachmentFilenameIsRequired, err, "[TestModelAttachmentSuccess] Error should \"attachment: Filename is required\"")
}

func TestModelAttachmentBodyIsRequired(t *testing.T) {
	mdl := remailer.Attachment{
		Filename: "test.txt",
		Body:     nil,
	}

	err := mdl.Validate()

	assert.Error(t, err, "[TestModelAttachmentBodyIsRequired] Should error")
	assert.Equal(t, remailer.ErrAttachmentBodyIsRequired, err, "[TestModelAttachmentBodyIsRequired] Error should \"attachment: Body is required\"")
}
