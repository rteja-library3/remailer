package remailer

import (
	"net/smtp"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ses"
)

type SESClientBuilder func(configs ...*aws.Config) (client SESClient, err error)
type SMTPClient func(addr string, a smtp.Auth, from string, to []string, msg []byte) error

var (
	DefaultSESClientBuilder SESClientBuilder = func(configs ...*aws.Config) (client SESClient, err error) {
		sess, err := session.NewSession(configs...)
		if err != nil {
			return
		}

		// Create an SES session.
		client = ses.New(sess)
		return
	}
	DefaultSMTPClient SMTPClient = smtp.SendMail
)
